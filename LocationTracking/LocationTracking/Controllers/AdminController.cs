﻿using LocationTracking.Common_Functions;
using LocationTracking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LocationTracking.Controllers
{
    public class AdminController : Controller
    {
        APIFunctionCall _APICall = new APIFunctionCall();
        List<MUO_AdminUsers> _AdminInfoList = new List<MUO_AdminUsers>();
        #region Admin Login 

        #region Admin Login View 
        [HttpGet]
        public ActionResult AdminLogin()
        {
            return View();
        }
        #endregion

        #region Check Admin Credentials and Forget Password
        [HttpPost]
        public ActionResult AdminLogin(AdminLogin models,string submit)
        {
            if(submit == "Login")
            {
                //try
                //{
                //  var adminInfoList= _APICall.CheckAdminCredential(models);
                //    if (adminInfoList.Count != 0)
                //    {
                //        MUO_AdminUsers AdminInfo = new MUO_AdminUsers()
                //        {
                //            UserID = adminInfoList[0].UserID
                //        };
                //        _AdminInfoList.Add(AdminInfo);
                //        return RedirectToAction("AdminIndex");
                //    }
                //    else
                //    {
                //        return RedirectToAction("AdminLogin");
                //    }
                //}
                //catch(Exception ex)
                //{
                //    string erMsg = ex.Message.ToString();
                //}
             
            }
            else if(submit == "Forget Password")
            {
                try
                {

                }
                catch (Exception ex)
                {
                    string erMsg = ex.Message.ToString();
                }
                return  RedirectToAction("ForgetPassword");
            }
            return View("AdminLogin");
        }
        #endregion

        #region Admin Index Page
        public ActionResult AdminIndex()
        {
            return View();
        }
        #endregion

        #region Forget Password View
        public ActionResult ForgetPassword()
        {
            return View();
        }
        #endregion

        #endregion
    }
}