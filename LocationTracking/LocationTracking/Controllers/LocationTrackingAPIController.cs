﻿using LocationTracking.Common_Functions;
using LocationTracking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LocationTracking.Controllers
{
    [RoutePrefix("api/LocationTrackingAPI")]
    public class LocationTrackingAPIController : ApiController
    {
       
        List<MUO_TrackingUsers> _AdminInfoList = new List<MUO_TrackingUsers>();
        List<MUO_TrackingUserMessage> _NotificationList = new List<MUO_TrackingUserMessage>();
        LocationTrackingEntities _DataBase = new LocationTrackingEntities();
        List<MUO_Tracking> _UserLocationList = new List<MUO_Tracking>();
        List<MUO_TrackingAuthorization> _WeAreTrackingMobileList = new List<MUO_TrackingAuthorization>();

        #region For Receive Check User Credentials and return User ID
        [HttpPost]
        [ActionName("UserLogin")]
        public List<MUO_TrackingUsers> UserLogin(MUO_TrackingUsers model)
        {
            var _getUserInfo = _DataBase.MUO_TrackingUsers.Where(x => x.Mobile == model.Mobile && x.Password == model.Password).ToList();
            if (_getUserInfo.Count != 0)
            {
                MUO_TrackingUsers AdminInfo = new MUO_TrackingUsers()
                {
                    UserID = _getUserInfo[0].UserID                    
                };
                _AdminInfoList.Add(AdminInfo);
            }
            return _AdminInfoList;
        }
        #endregion

        #region For Save New User Information / Sign UP
        [HttpPost]
        [ActionName("UserRegistration")]
        public List<MUO_TrackingUsers> UserRegistration(MUO_TrackingUsers model)
        {
            _AdminInfoList.Clear();
            try
            {
                var Query_Databse = (from x in _DataBase.MUO_TrackingUsers where x.Mobile == model.Mobile select x).ToList();
                if(Query_Databse.Count==0)
                {
                    _DataBase.MUO_TrackingUsers.Add(model);
                    _DataBase.SaveChanges();
                    var _getUserInfo = _DataBase.MUO_TrackingUsers.Where(x => x.Mobile == model.Mobile).ToList();
                    if (_getUserInfo.Count != 0)
                    {
                        MUO_TrackingUsers AdminInfo = new MUO_TrackingUsers()
                        {
                            UserID = _getUserInfo[0].UserID
                        };
                        _AdminInfoList.Add(AdminInfo);
                    }
                }
                else
                {
                    MUO_TrackingUsers AdminInfo = new MUO_TrackingUsers()
                    {
                        UserID = 0
                    };
                    _AdminInfoList.Add(AdminInfo);
                }
               
            }
            catch(Exception ex)
            {

            }
           
            return _AdminInfoList;
        }

        #endregion

        #region For Save User Location Information Every Minute
        [HttpPost]
        [ActionName("SaveUserLocationInformation")]
        public int SaveUserLocationInformation(MUO_Tracking model)
        {
            int _LocationSaved = 0;
            try
            {
                _DataBase.MUO_Tracking.Add(model);
                _DataBase.SaveChanges();
                _LocationSaved = 1;
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

            return _LocationSaved;
        }
        #endregion

        #region For Check Notification Regularly if New Notification Available Send Number Of New Notifications
        [HttpPost]
        [ActionName("CheckUserNotifications")]
        public int CheckUserNotifications(MUO_TrackingUserMessage model)
        {
            int _NotificationCount = 0;
            try
            {
                var NotificationList = _DataBase.MUO_TrackingUserMessage.Where(x => x.ReceiverID == model.ReceiverID && x.MessageMode == model.MessageMode).ToList();
                if(NotificationList.Count!=0)
                {
                    _NotificationCount = NotificationList.Count;
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

            return _NotificationCount;
        }
        #endregion

        #region For Get All Notification
        [HttpPost]
        [ActionName("GetAllNotifications")]
        public List<MUO_TrackingUserMessage> GetAllNotifications(MUO_TrackingUserMessage model)
        {
            _NotificationList.Clear();
            try
            {
                var NotificationList = _DataBase.MUO_TrackingUserMessage.Where(x => x.ReceiverID == model.ReceiverID).ToList();
                if (NotificationList.Count != 0)
                {
                    for(int i=0;i<NotificationList.Count;i++)
                    {
                        MUO_TrackingUserMessage _NotificationItem = new MUO_TrackingUserMessage()
                        {
                            MessageMode = NotificationList[i].MessageMode,
                            MessageID=NotificationList[i].MessageID,
                            AddedDate=NotificationList[i].AddedDate,
                            IsRead=NotificationList[i].IsRead,
                            ReadDate=NotificationList[i].ReadDate,
                            ReceiverID=NotificationList[i].ReceiverID,
                            SenderID=NotificationList[i].SenderID
                        };
                        _NotificationList.Add(_NotificationItem);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

            return _NotificationList;
        }
        #endregion

        #region Get User Information By  ID
        [HttpPost]
        [ActionName("GetUserInformation")]
        public List<MUO_TrackingUsers> GetUserInformation(MUO_TrackingUsers model)
        {
            _AdminInfoList.Clear();
            try
            {
                var Query_Databse = _DataBase.MUO_TrackingUsers.Where(x => x.UserID == model.UserID).ToList();
                if (Query_Databse.Count != 0)
                {
                    MUO_TrackingUsers itm = new MUO_TrackingUsers()
                    {
                        UserID=Query_Databse[0].UserID,
                        FirstName=Query_Databse[0].FirstName,
                        LastName=Query_Databse[0].LastName,
                        Mobile=Query_Databse[0].Mobile
                    };
                    _AdminInfoList.Add(itm);
                }               
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

            return _AdminInfoList;
        }
        #endregion

        #region Get User Information By  Mobile Number
        [HttpPost]
        [ActionName("GetUserInformationByMobile")]
        public List<MUO_TrackingUsers> GetUserInformationByMobile(MUO_TrackingUsers model)
        {
            _AdminInfoList.Clear();
            try
            {
                var Query_Databse = _DataBase.MUO_TrackingUsers.Where(x => x.Mobile == model.Mobile).ToList();
                if (Query_Databse.Count != 0)
                {
                    MUO_TrackingUsers itm = new MUO_TrackingUsers()
                    {
                        UserID = Query_Databse[0].UserID,
                        FirstName = Query_Databse[0].FirstName,
                        LastName = Query_Databse[0].LastName,
                        Mobile = Query_Databse[0].Mobile
                    };
                    _AdminInfoList.Add(itm);
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

            return _AdminInfoList;
        }
        #endregion

        #region Get User Location Details By User ID and Current Date Split Pending
        [HttpPost]
        [ActionName("GetUserLocationInformation")]
        public List<MUO_Tracking> GetUserLocationInformation(MUO_Tracking model)
        {
            _UserLocationList.Clear();
            try
            {
                var _userLocationList = _DataBase.MUO_Tracking.Where(x => x.UserID == model.UserID).ToList();
                if(_userLocationList.Count!=0)
                {
                    for(int i=0;i<_userLocationList.Count;i++)
                    {
                        MUO_Tracking mUO_Tracking = new MUO_Tracking()
                        {
                            Longitude=_userLocationList[i].Longitude,
                            LocationDatetime=_userLocationList[i].LocationDatetime,
                            Latitude=_userLocationList[i].Latitude,
                            Speed=_userLocationList[i].Speed,
                            Bearing=_userLocationList[i].Bearing,
                            Distance=_userLocationList[i].Distance
                        };
                        _UserLocationList.Add(mUO_Tracking);
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

            return _UserLocationList;
        }
        #endregion

        #region Get First Location Details From Server By User ID
        [HttpPost]
        [ActionName("GetFirstLocationInformation")]
        public List<MUO_Tracking> GetFirstLocationInformation(MUO_Tracking model)
        {
            _UserLocationList.Clear();
            try
            {
                var _userLocationList = _DataBase.MUO_Tracking.FirstOrDefault(x => x.UserID == model.UserID);
                MUO_Tracking mUO_Tracking = new MUO_Tracking()
                {
                    Longitude = _userLocationList.Longitude,
                    LocationDatetime = _userLocationList.LocationDatetime,
                    Latitude = _userLocationList.Latitude,
                    Speed = _userLocationList.Speed,
                    Bearing = _userLocationList.Bearing,
                    Distance = _userLocationList.Distance
                };
                _UserLocationList.Add(mUO_Tracking);
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

            return _UserLocationList;
        }
        #endregion

        #region    Get We Are Tracking Mobile List
        [HttpPost]
        [ActionName("GetWeAreTrackingMobile")]
        public List<MUO_TrackingAuthorization> GetWeAreTrackingMobile(MUO_TrackingAuthorization model)
        {
            _WeAreTrackingMobileList.Clear();
            try
            {
                var _weAreTrackingList = _DataBase.MUO_TrackingAuthorization.Where(x => x.P_UserID == model.P_UserID).ToList();
                if (_weAreTrackingList.Count != 0)
                {
                    for (int i = 0; i < _weAreTrackingList.Count; i++)
                    {
                        MUO_TrackingAuthorization mUO_TrackingAuthorization = new MUO_TrackingAuthorization()
                        {
                            P_UserID = _weAreTrackingList[i].P_UserID,
                            C_UserID = _weAreTrackingList[i].C_UserID,
                            AuthorizedDate = _weAreTrackingList[i].AuthorizedDate,
                            IsAuthorized = _weAreTrackingList[i].IsAuthorized,
                            IsDeleted = _weAreTrackingList[i].IsDeleted,
                            LinkedID = _weAreTrackingList[i].LinkedID
                        };
                        _WeAreTrackingMobileList.Add(mUO_TrackingAuthorization);
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

            return _WeAreTrackingMobileList;
        }
        #endregion

        #region    Delete User from We Are Tracking Mobile List
        [HttpPost]
        [ActionName("DeleteUserFromWeAreTracking")]
        public int DeleteUserFromWeAreTracking(MUO_TrackingAuthorization model)
        {
            int status = 0;
            try
            {
                var QueryData = (from x in _DataBase.MUO_TrackingAuthorization
                                 where x.C_UserID == model.C_UserID && x.P_UserID==model.P_UserID
                                 select x).First();
                _DataBase.MUO_TrackingAuthorization.Remove(QueryData);
                _DataBase.SaveChanges();
                status = 1;
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return status;
        }
        #endregion
                
        #region For Add records in Notifications and We Are Tracking Table when User Send Request
        [HttpPost]
        [ActionName("SendRequestForAuthentication")]
        public int SendRequestForAuthentication(Model_SendUserAuthRequest model)
        {
            int status = 0;
            try
            {
                var Query_DatabseForRegisterNomberOrNot = (from x in _DataBase.MUO_TrackingUsers where x.Mobile == model.Mobile select x).ToList();
                if (Query_DatabseForRegisterNomberOrNot.Count != 0)
                {
                    long _c_userID = Query_DatabseForRegisterNomberOrNot[0].UserID;
                    var _checkAuthorization = _DataBase.MUO_TrackingAuthorization.Where(x => x.P_UserID == model.SenderID 
                    && x.C_UserID == _c_userID).ToList();
                    if(_checkAuthorization.Count==0)
                    {
                        var _checkPrvNotification = _DataBase.MUO_TrackingUserMessage.Where(x => x.SenderID == model.SenderID && x.ReceiverID == _c_userID).ToList();
                        if(_checkPrvNotification.Count!=0)
                        {                          
                       
                            MUO_TrackingUserMessage mUO_TrackingUserMessage = new MUO_TrackingUserMessage()
                            {
                                ReceiverID = Query_DatabseForRegisterNomberOrNot[0].UserID,
                                SenderID = model.SenderID,
                                MessageMode = "0",
                                IsRead = false,
                                AddedDate = DateTime.UtcNow,
                                ReadDate=DateTime.UtcNow
                            };
                            _DataBase.MUO_TrackingUserMessage.Add(mUO_TrackingUserMessage);
                            _DataBase.SaveChanges();

                            MUO_TrackingAuthorization mUO_TrackingAuthorization = new MUO_TrackingAuthorization()
                            {
                                P_UserID = model.SenderID,
                                C_UserID = Query_DatabseForRegisterNomberOrNot[0].UserID,
                                IsAuthorized = false,
                                IsDeleted=false,
                                AuthorizedDate=DateTime.UtcNow                                
                            };
                            _DataBase.MUO_TrackingAuthorization.Add(mUO_TrackingAuthorization);
                            _DataBase.SaveChanges();
                            status = 5;

                        }
                    }
                    else
                    {
                        status = 2;
                    }
                   
                }
                else
                {
                    status = 1;
                }

            }
            catch (Exception ex)
            {
                string sy = ex.Message.ToString();
            }

            return status;
        }
        #endregion

        #region For  Send Request Allow / Deny or Accept / Reject
        [HttpPost]
        [ActionName("SendTrackingRequest")]
        public int SendTrackingRequest(Model_SendUserAuthRequest model)
        {
            int status = 0;
            try
            {
                var Query_in_TrackingUserMessage = (from x in _DataBase.MUO_TrackingUserMessage
                                                    where x.MessageID == model.MessageID select x).First();
                Query_in_TrackingUserMessage.MessageMode = model.MessageMode;
                Query_in_TrackingUserMessage.IsRead = model.Isread;
                Query_in_TrackingUserMessage.ReadDate = DateTime.UtcNow;
                _DataBase.SaveChanges();
                var _SenderMobile = _DataBase.MUO_TrackingUsers.Where(x => x.Mobile == model.Mobile).ToList();
                if(_SenderMobile.Count!=0)
                {
                    long Receiverid = _SenderMobile[0].UserID;
                    var Query_in_TrackingAuthorization = (from x in _DataBase.MUO_TrackingAuthorization
                                                          where x.P_UserID ==Receiverid && x.C_UserID ==model.SenderID
                                                          select x).First();
                    Query_in_TrackingAuthorization.IsAuthorized = model.IsAuthorized;
                    Query_in_TrackingAuthorization.AuthorizedDate = DateTime.UtcNow;
                    _DataBase.SaveChanges();
                    status = 1;
                }
               
            }
            catch (Exception ex)
            {
                string sy = ex.Message.ToString();
            }

            return status;
        }
        #endregion

        #region Get We Are Tracked By List
        [HttpPost]
        [ActionName("GetWeAreTrackedByMobile")]
        public List<MUO_TrackingAuthorization> GetWeAreTrackedByMobile(MUO_TrackingAuthorization model)
        {
            _WeAreTrackingMobileList.Clear();
            try
            {
                var _weAreTrackingList = _DataBase.MUO_TrackingAuthorization.Where(x => x.C_UserID == model.C_UserID).ToList();
                if (_weAreTrackingList.Count != 0)
                {
                    for (int i = 0; i < _weAreTrackingList.Count; i++)
                    {
                        MUO_TrackingAuthorization mUO_TrackingAuthorization = new MUO_TrackingAuthorization()
                        {
                            P_UserID = _weAreTrackingList[i].P_UserID,
                            C_UserID = _weAreTrackingList[i].C_UserID,
                            AuthorizedDate = _weAreTrackingList[i].AuthorizedDate,
                            IsAuthorized = _weAreTrackingList[i].IsAuthorized,
                            IsDeleted = _weAreTrackingList[i].IsDeleted,
                            LinkedID = _weAreTrackingList[i].LinkedID
                        };
                        _WeAreTrackingMobileList.Add(mUO_TrackingAuthorization);
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

            return _WeAreTrackingMobileList;
        }
        #endregion

        #region Delete We Are Tracked By List
        [HttpPost]
        [ActionName("DeleteUserFromWeAreTrackedBy")]
        public int DeleteUserFromWeAreTrackedBy(MUO_TrackingAuthorization model)
        {
            int status = 0;
            try
            {
                var QueryData = (from x in _DataBase.MUO_TrackingAuthorization
                                 where x.C_UserID == model.C_UserID && x.P_UserID == model.P_UserID
                                 select x).First();
                _DataBase.MUO_TrackingAuthorization.Remove(QueryData);
                _DataBase.SaveChanges();
                status = 1;
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }
            return status;
        }
        #endregion

        #region For  Add Tracking Status
        [HttpPost]
        [ActionName("AddTrackingStatus")]
        public int AddTrackingStatus(MUO_TrackingOnOff model)
        {
            int status = 0;
            try
            {
                _DataBase.MUO_TrackingOnOff.Add(model);
                _DataBase.SaveChanges();
                status = 1;
            }
            catch (Exception ex)
            {
                string sy = ex.Message.ToString();
            }

            return status;
        }
        #endregion

        #region For  Change Tracking Status
        [HttpPost]
        [ActionName("ChangeTrackingStatus")]
        public int ChangeTrackingStatus(Model_TrackingStatus model)
        {
            int status = 0;
            try
            {
                var _GetUserInfo = _DataBase.MUO_TrackingUsers.Where(x => x.Mobile == model.Mobile).ToList();
                if(_GetUserInfo.Count!=0)
                {
                    long _P_UserID = _GetUserInfo[0].UserID;
                    var Query_in_TrackingStatus = (from x in _DataBase.MUO_TrackingOnOff
                                                   where x.P_UserID == model.C_UserID && x.C_UserID==_P_UserID
                                                   select x).First();
                    Query_in_TrackingStatus.IsOn = model.IsOn;
                    Query_in_TrackingStatus.OffDate = model.OffDate;
                    Query_in_TrackingStatus.OnDate = model.OnDate;
                    _DataBase.SaveChanges();
                    status = 1;
                }
               
            }
            catch (Exception ex)
            {
                string sy = ex.Message.ToString();
            }

            return status;
        }
        #endregion

    }
}
