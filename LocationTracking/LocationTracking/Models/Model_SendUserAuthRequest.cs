﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocationTracking.Models
{
    public class Model_SendUserAuthRequest
    {
        public int MessageID { get; set; }
        public int SenderID { get; set; }
        public int ReceiverID { get; set; }
        public string MessageMode { get; set; }
        public string Mobile { get; set; }
        public bool Isread { get; set; }
        public DateTime AddedDate { get; set; }
        public bool IsAuthorized { get; set; }
    }
}

              