//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LocationTracking.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MUO_TrackingUsers
    {
        public long UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Mobile { get; set; }
        public string OTP { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public Nullable<System.DateTime> ConfirmDate { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string UserImage { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsEmailVerified { get; set; }
        public Nullable<System.DateTime> EmailVerificationDate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string Country { get; set; }
        public Nullable<bool> DeletedBy { get; set; }
        public Nullable<bool> DeletedDate { get; set; }
        public string NewEmailID { get; set; }
        public Nullable<bool> IsLoggedIn { get; set; }
        public Nullable<System.DateTime> LoginTime { get; set; }
        public string IPAddress { get; set; }
        public Nullable<bool> IsFree { get; set; }
        public Nullable<System.DateTime> FreeUpto { get; set; }
        public Nullable<bool> PackagePurchased { get; set; }
        public Nullable<int> PackageID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> UsersAllow { get; set; }
        public Nullable<long> ParentID { get; set; }
    }
}
