//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LocationTracking.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MUO_ContactUs
    {
        public long ContactId { get; set; }
        public string Subject { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comments { get; set; }
        public Nullable<System.DateTime> PostedDate { get; set; }
        public Nullable<bool> IsArchive { get; set; }
        public Nullable<System.DateTime> ArchiveDate { get; set; }
        public Nullable<int> ArchivedBy { get; set; }
    }
}
