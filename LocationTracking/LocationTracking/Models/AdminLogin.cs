﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LocationTracking.Models
{
    public class AdminLogin
    {
        [Required(ErrorMessage ="User Name is Required")]
        public string UserName { get; set; }

        [Required(ErrorMessage ="Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}