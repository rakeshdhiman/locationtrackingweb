﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocationTracking.Models
{
    public class Model_TrackingStatus
    {
        public long OnOffID { get; set; }
        public long C_UserID { get; set; }
        public long P_UserID { get; set; }
        public bool IsOn { get; set; }
        public string Mobile { get; set; }
        public DateTime OffDate { get; set; }
        public DateTime OnDate { get; set; }
    }
}