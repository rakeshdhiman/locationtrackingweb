//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LocationTracking.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MUO_OrderItemsTemp
    {
        public long OrderItemID { get; set; }
        public long PackageID { get; set; }
        public long UserID { get; set; }
        public Nullable<long> TempID { get; set; }
        public string PackageName { get; set; }
        public Nullable<int> Status { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<decimal> GST { get; set; }
    }
}
